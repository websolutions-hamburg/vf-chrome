@echo off

>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if %errorlevel% NEQ 0 (
	echo You must run this file with administrative privileges
	echo.
	pause
	exit /B
) else ( 
	echo Disable Google Chrome Policies v2.0
	echo ===================================
	echo.
	echo.
	goto run
)




:run
goto deaktivate




:deaktivate
echo 1/3 - enable policies
echo ---------------------
echo.

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Group Policy\{35378EAC-683F-11D2-A89A-00C04FBBCFA2}" /V "NoBackgroundPolicy" /t REG_DWORD /d 0 /f

if %errorlevel% NEQ 0 (
	echo.
	echo Error :-^(
	echo.
	pause
	exit /B
) else (
	echo.
	echo.
	echo 2/3 - update policies
	echo ---------------------
	echo.
	gpupdate
)

goto activate




:activate
echo.
echo 3/3 - disable policies
echo ----------------------
echo.

reg delete HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome /f 2>nul
reg delete HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Update /f 2>nul

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Group Policy\{35378EAC-683F-11D2-A89A-00C04FBBCFA2}" /V "NoBackgroundPolicy" /t REG_DWORD /d 1 /f

echo.

if %errorlevel% NEQ 0 (
	echo.
	echo Error :-^(
	echo.
	pause
	exit /B
)