# VF-Chrome #

Turn off Google Chrome Policies *(everything at your own risk, I assume no liability)*


## Installation ##

1. Download the [bundle.zip](https://bitbucket.org/websolutions-hamburg/vf-chrome/downloads/bundle.zip)
2. Unzip the archiv to a folder (maybe not your user folder) of your choice. (You will find the PWD in the [Helper Section](#markdown-header-helper))
3. *(Run **Reset old v1.0 version.exe** with your **a6** account if you used the old "turn off policies" version)*
4. Open the **Task Scheduler** (open the **Run** window [Windows]+[R] and type **Taskschd.msc**).
5. *(See [Task Scheduler.md](https://bitbucket.org/websolutions-hamburg/vf-chrome/src/6d4c773f8233fe56438e304001c6cadd3ae454c8/Task%20Scheduler.md?at=master&fileviewer=file-view-default) for more help)*
6. Left mouse click on **Task scheduling library** and then right mouse click on **Task scheduling library**.
7. Select **Create Basic Task**.
8. Enter a name, for example "Turn Off Chrome Policies" and press next.
9. Select **When I log on** and press next.
10. Select **Start a program** and press next.
11. Click on **Browse** button and select **Turn Off Chrome Policies.exe** and press next.
12. Click on **Finish**.
13. Re-login in Windows.
14. Enter your **a6** account data when the system ask you for administrative privileges for the scheduled task.


## Uninstallation ##
1. Delete the scheduling task. (Run **Taskschd.msc** with your **a6** account, see [Installation section](#markdown-header-installation))
2. Run **Turn On Chrome Policies.exe**.


## Helper ##
- PWD: chrome
- [Convert .bat to .exe](http://www.f2ko.de/apps/ob2e/)