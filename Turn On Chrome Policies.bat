@echo off

>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if %errorlevel% NEQ 0 (
	echo You must run this file with administrative privileges
	echo.
	pause
	exit /B
) else (
	goto run
)




:run
echo Reactivate Google Chrome Policies v2.0
echo ======================================
echo.
echo.

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Group Policy\{35378EAC-683F-11D2-A89A-00C04FBBCFA2}" /V "NoBackgroundPolicy" /t REG_DWORD /d 0 /f

echo.

gpupdate
	
echo.
echo Done :-^)
echo.

pause