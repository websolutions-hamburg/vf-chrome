@echo off

>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if %errorlevel% NEQ 0 (
	echo You must run this file with administrative privileges
	echo.
	pause
	exit /B
) else (
	goto run
)




:run
echo Reset old v1.0 Google Chrome Policies deactivation
echo ==================================================
echo.

reg delete HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google /f

echo.

gpupdate

echo.
echo Done :-^)
echo.

pause